using UnityEngine;
using UnityEngine.Audio;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Audio)]
	public class TransitionToSnapshot : FsmStateAction
	{
	[RequiredField]
	[UIHint(UIHint.Variable)]
	public FsmBool boolVariable;

	[RequiredField]
	public AudioMixerSnapshot AudioSnapshotA;
	[RequiredField]
	public AudioMixerSnapshot AudioSnapshotB;
	[RequiredField]
	[UIHint(UIHint.Variable)]
	public FsmFloat TransitionTime;

	public override void Reset()
	{
			boolVariable = null;
			TransitionTime = null;
			AudioSnapshotB = null;
			AudioSnapshotA = null;
	}

	public override void OnEnter()
	{
		DoBoolOperator();
		Finish();
	}

	void DoBoolOperator()
	{
		var Bool = boolVariable.Value;
		var _TransitionTime = TransitionTime.Value;

		if(!Bool){
				AudioSnapshotB.TransitionTo(_TransitionTime);
			} else {
				AudioSnapshotA.TransitionTo(_TransitionTime);
			}

			boolVariable.Value = !boolVariable.Value;

		}

	}
}

