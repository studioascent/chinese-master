using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Unity.SharedVariables
{
    [TaskCategory("Unity/SharedVariable")]
    [TaskDescription("MODIFIED: Returns task status running when values equal.")]
    public class CompareSharedGameObject : Conditional
    {
        [Tooltip("The first variable to compare")]
        public SharedGameObject variable;
        [Tooltip("The variable to compare to")]
        public SharedGameObject compareTo;

        public override TaskStatus OnUpdate()
        {
            if (variable.Value == null && compareTo.Value != null)
                return TaskStatus.Failure;
            if (variable.Value == null && compareTo.Value == null)
                return TaskStatus.Running;

            return variable.Value.Equals(compareTo.Value) ? TaskStatus.Running : TaskStatus.Failure;
        }

        public override void OnReset()
        {
            variable = null;
            compareTo = null;
        }
    }
}