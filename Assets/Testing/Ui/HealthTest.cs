﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthTest : MonoBehaviour
{
    public int health;
    public int numPips;

    public Image[] pips;
    public Sprite fullPip;
    public Sprite emptyPip;

    private void Update()
    {
        if(health > numPips)
        {
            health = numPips;
        }
            
        for(int i = 0; i < pips.Length; i++)
        {
            if(i < health)
            {
                pips[i].sprite = fullPip;
            }

            else
            {
                pips[i].sprite = emptyPip;
            }

            if (i < numPips)
            {
                pips[i].enabled = true;
            }

            else
            {
                pips[i].enabled = false;
            }
        }
    }

}
