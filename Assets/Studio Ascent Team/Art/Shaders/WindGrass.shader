// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/WindGrass"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.21
		_WindStrengthDirectionMultiplier("WindStrengthDirectionMultiplier", Vector) = (0,0,0,0)
		_BendPower("Bend Power", Range( 1 , 2)) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_ColourTint("Colour Tint", Color) = (1,1,1,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform sampler2D _WindNoiseTexture;
		uniform float _WindSpeed;
		uniform float _WindNoiseScale;
		uniform float _WindNoiseStrength;
		uniform float4 _WindDirection;
		uniform float _WindDirectionStrength;
		uniform float _BendPower;
		uniform float3 _WindStrengthDirectionMultiplier;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _ColourTint;
		uniform float _Cutoff = 0.21;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 temp_cast_0 = (_WindSpeed).xx;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 appendResult594 = (float4(ase_worldPos.x , ase_worldPos.y , 0.0 , 0.0));
			float2 panner598 = ( 1.0 * _Time.y * temp_cast_0 + ( appendResult594 * _WindNoiseScale ).xy);
			float3 appendResult599 = (float3(_WindDirection.x , _WindDirection.y , _WindDirection.z));
			float3 worldToObjDir604 = mul( unity_WorldToObject, float4( appendResult599, 0 ) ).xyz;
			v.vertex.xyz += ( ( ( (tex2Dlod( _WindNoiseTexture, float4( panner598, 0, 0.0) )*2.0 + -1.0) * _WindNoiseStrength ) + float4( ( worldToObjDir604 * _WindDirectionStrength ) , 0.0 ) ) * pow( v.color.a , _BendPower ) * float4( _WindStrengthDirectionMultiplier , 0.0 ) ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode614 = tex2D( _TextureSample0, uv_TextureSample0 );
			o.Albedo = ( tex2DNode614 * _ColourTint ).rgb;
			o.Alpha = 1;
			clip( tex2DNode614.a - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17700
394;73;1218;656;49.86548;-352.9672;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;613;-1008.375,889.0035;Inherit;False;1905.437;1038.998;Tied to WIND CONTROL game object;21;612;611;610;609;608;607;605;606;604;602;603;601;599;600;597;598;595;596;593;594;592;Wind Displacement;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;592;-961.6241,1276.859;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;593;-699.5142,1443.864;Float;False;Global;_WindNoiseScale;_WindNoiseScale;8;0;Create;True;0;0;False;0;-0.15;0.575;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;594;-684.7172,1274.276;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;596;-468.3811,1285.412;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;595;-684.9481,1585.915;Float;False;Global;_WindSpeed;_WindSpeed;8;0;Create;True;0;0;False;0;-0.15;0.055;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;598;-294.3315,1228.911;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector4Node;597;-935.7911,1039.654;Float;False;Global;_WindDirection;_WindDirection;25;0;Create;True;0;0;False;0;0,0,0,0;-0.9178746,0,-0.396871,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;600;-78.36927,1140.033;Inherit;True;Global;_WindNoiseTexture;_WindNoiseTexture;1;0;Create;True;0;0;False;0;-1;None;6373380e25671b142917e356b5f57d76;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;599;-618.7952,1035.441;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;601;62.67956,1058.247;Inherit;False;Global;_WindDirectionStrength;_WindDirectionStrength;14;0;Create;True;0;0;False;0;0;0.029;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;603;240.0909,1183.694;Inherit;False;3;0;COLOR;0,0,0,0;False;1;FLOAT;2;False;2;FLOAT;-1;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;602;148.6646,1433.834;Inherit;False;Global;_WindNoiseStrength;_WindNoiseStrength;14;0;Create;True;0;0;False;0;0;0.041;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TransformDirectionNode;604;-311.904,982.3597;Inherit;False;World;Object;False;Fast;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;607;482.5952,1179.331;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;606;362.6193,979.3697;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;608;-261.8038,1643.082;Inherit;False;Property;_BendPower;Bend Power;3;0;Create;True;0;0;False;0;0;1.9;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;605;-228.1501,1455.208;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;615;546.1345,688.9672;Inherit;False;Property;_ColourTint;Colour Tint;5;0;Create;True;0;0;False;0;1,1,1,0;1,0.8741625,0.4292453,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;614;496.0201,458.0266;Inherit;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;-1;None;980e9ebf3cc631d418ac6d7237cedbed;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;611;-178.8716,1750.386;Inherit;False;Property;_WindStrengthDirectionMultiplier;WindStrengthDirectionMultiplier;2;0;Create;True;0;0;False;0;0,0,0;-0.5,1.74,0.2;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;609;600.6279,1074.417;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;610;160.6964,1503.982;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;612;678.5609,1304.816;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;616;831.1345,637.9672;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;114;1073.315,603.1606;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/WindGrass;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.21;True;True;0;False;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Spherical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;594;0;592;1
WireConnection;594;1;592;2
WireConnection;596;0;594;0
WireConnection;596;1;593;0
WireConnection;598;0;596;0
WireConnection;598;2;595;0
WireConnection;600;1;598;0
WireConnection;599;0;597;1
WireConnection;599;1;597;2
WireConnection;599;2;597;3
WireConnection;603;0;600;0
WireConnection;604;0;599;0
WireConnection;607;0;603;0
WireConnection;607;1;602;0
WireConnection;606;0;604;0
WireConnection;606;1;601;0
WireConnection;609;0;607;0
WireConnection;609;1;606;0
WireConnection;610;0;605;4
WireConnection;610;1;608;0
WireConnection;612;0;609;0
WireConnection;612;1;610;0
WireConnection;612;2;611;0
WireConnection;616;0;614;0
WireConnection;616;1;615;0
WireConnection;114;0;616;0
WireConnection;114;10;614;4
WireConnection;114;11;612;0
ASEEND*/
//CHKSM=EA30B69B55543BE42A99DFF2F96FEC515DC630E1