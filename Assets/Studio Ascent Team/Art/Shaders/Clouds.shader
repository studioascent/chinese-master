// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Clouds"
{
	Properties
	{
		_EdgeLength ( "Edge length", Range( 2, 50 ) ) = 15
		_NoiseRemap("Noise Remap", Vector) = (0,0,0,0)
		_ColorB("ColorB", Color) = (1,1,1,1)
		_ColorA("ColorA", Color) = (0,0,0,1)
		_NoisePower("Noise Power", Range( 0 , 3)) = 1
		_BaseScale("BaseScale", Range( 0 , 10)) = 5
		_BaseStrength("BaseStrength", Range( 0 , 10)) = 5
		_EmissionStrength("Emission Strength", Range( 0 , 10)) = 5
		_FadeDepth("Fade Depth", Range( 0 , 100)) = 1
		_RotateProjection("Rotate Projection", Vector) = (0,0,0,0)
		_NoiseScale("Noise Scale", Range( 0 , 1)) = 1
		_Speed("Speed", Range( 0 , 110)) = 100
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform float4 _RotateProjection;
		uniform float _Speed;
		uniform float _NoiseScale;
		uniform float _NoisePower;
		uniform float4 _NoiseRemap;
		uniform float _BaseScale;
		uniform float _BaseStrength;
		uniform float4 _ColorA;
		uniform float4 _ColorB;
		uniform float _EmissionStrength;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float _FadeDepth;
		uniform float _EdgeLength;


		float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
		{
			original -= center;
			float C = cos( angle );
			float S = sin( angle );
			float t = 1 - C;
			float m00 = t * u.x * u.x + C;
			float m01 = t * u.x * u.y - S * u.z;
			float m02 = t * u.x * u.z + S * u.y;
			float m10 = t * u.x * u.y + S * u.z;
			float m11 = t * u.y * u.y + C;
			float m12 = t * u.y * u.z - S * u.x;
			float m20 = t * u.x * u.z - S * u.y;
			float m21 = t * u.y * u.z + S * u.x;
			float m22 = t * u.z * u.z + C;
			float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
			return mul( finalMatrix, original ) + center;
		}


		float2 UnityGradientNoiseDir( float2 p )
		{
			p = fmod(p , 289);
			float x = fmod((34 * p.x + 1) * p.x , 289) + p.y;
			x = fmod( (34 * x + 1) * x , 289);
			x = frac( x / 41 ) * 2 - 1;
			return normalize( float2(x - floor(x + 0.5 ), abs( x ) - 0.5 ) );
		}
		
		float UnityGradientNoise( float2 UV, float Scale )
		{
			float2 p = UV * Scale;
			float2 ip = floor( p );
			float2 fp = frac( p );
			float d00 = dot( UnityGradientNoiseDir( ip ), fp );
			float d01 = dot( UnityGradientNoiseDir( ip + float2( 0, 1 ) ), fp - float2( 0, 1 ) );
			float d10 = dot( UnityGradientNoiseDir( ip + float2( 1, 0 ) ), fp - float2( 1, 0 ) );
			float d11 = dot( UnityGradientNoiseDir( ip + float2( 1, 1 ) ), fp - float2( 1, 1 ) );
			fp = fp * fp * fp * ( fp * ( fp * 6 - 15 ) + 10 );
			return lerp( lerp( d00, d01, fp.y ), lerp( d10, d11, fp.y ), fp.x ) + 0.5;
		}


		//https://www.shadertoy.com/view/XdXGW8
		float2 GradientNoiseDir( float2 x )
		{
			const float2 k = float2( 0.3183099, 0.3678794 );
			x = x * k + k.yx;
			return -1.0 + 2.0 * frac( 16.0 * k * frac( x.x * x.y * ( x.x + x.y ) ) );
		}
		
		float GradientNoise( float2 UV, float Scale )
		{
			float2 p = UV * Scale;
			float2 i = floor( p );
			float2 f = frac( p );
			float2 u = f * f * ( 3.0 - 2.0 * f );
			return lerp( lerp( dot( GradientNoiseDir( i + float2( 0.0, 0.0 ) ), f - float2( 0.0, 0.0 ) ),
					dot( GradientNoiseDir( i + float2( 1.0, 0.0 ) ), f - float2( 1.0, 0.0 ) ), u.x ),
					lerp( dot( GradientNoiseDir( i + float2( 0.0, 1.0 ) ), f - float2( 0.0, 1.0 ) ),
					dot( GradientNoiseDir( i + float2( 1.0, 1.0 ) ), f - float2( 1.0, 1.0 ) ), u.x ), u.y );
		}


		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityEdgeLengthBasedTess (v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
		}

		void vertexDataFunc( inout appdata_full v )
		{
			half localunity_ObjectToWorld0w1_g1 = ( unity_ObjectToWorld[0].w );
			half localunity_ObjectToWorld1w2_g1 = ( unity_ObjectToWorld[1].w );
			half localunity_ObjectToWorld2w3_g1 = ( unity_ObjectToWorld[2].w );
			float3 appendResult6_g1 = (float3(localunity_ObjectToWorld0w1_g1 , localunity_ObjectToWorld1w2_g1 , localunity_ObjectToWorld2w3_g1));
			float3 ase_vertexNormal = v.normal.xyz;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float temp_output_21_0 = degrees( _RotateProjection.w );
			float3 temp_cast_1 = (temp_output_21_0).xxx;
			float3 rotatedValue10 = RotateAroundAxis( degrees( _RotateProjection ).xyz, temp_cast_1, ase_worldPos, temp_output_21_0 );
			float2 temp_cast_3 = (( _Time.y * _Speed )).xx;
			float2 uv_TexCoord22 = v.texcoord.xy * rotatedValue10.xy + temp_cast_3;
			float temp_output_57_0 = ( _NoiseScale * 0.005 );
			float gradientNoise2 = UnityGradientNoise(uv_TexCoord22,temp_output_57_0);
			float gradientNoise37 = UnityGradientNoise(v.texcoord.xy,temp_output_57_0);
			float4 break46 = _NoiseRemap;
			float smoothstepResult52 = smoothstep( 0.0 , 1.0 , abs( (break46.x + (saturate( pow( saturate( ( ( gradientNoise2 + gradientNoise37 ) / 2.0 ) ) , ( 1 * _NoisePower ) ) ) - 0.0) * (break46.y - break46.x) / (1.0 - 0.0)) ));
			float2 temp_cast_5 = (( _Time.y * 0.2 )).xx;
			float2 uv_TexCoord66 = v.texcoord.xy * rotatedValue10.xy + temp_cast_5;
			float gradientNoise59 = GradientNoise(uv_TexCoord66,( 1 * _BaseScale ));
			gradientNoise59 = gradientNoise59*0.5 + 0.5;
			float temp_output_73_0 = ( 1 * _BaseStrength );
			float temp_output_74_0 = ( ( smoothstepResult52 + ( gradientNoise59 * temp_output_73_0 ) ) / ( 1.0 + temp_output_73_0 ) );
			v.vertex.xyz += ( appendResult6_g1 + ( ( ase_vertexNormal * temp_output_74_0 ) * 3 ) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float temp_output_21_0 = degrees( _RotateProjection.w );
			float3 temp_cast_1 = (temp_output_21_0).xxx;
			float3 rotatedValue10 = RotateAroundAxis( degrees( _RotateProjection ).xyz, temp_cast_1, ase_worldPos, temp_output_21_0 );
			float2 temp_cast_3 = (( _Time.y * _Speed )).xx;
			float2 uv_TexCoord22 = i.uv_texcoord * rotatedValue10.xy + temp_cast_3;
			float temp_output_57_0 = ( _NoiseScale * 0.005 );
			float gradientNoise2 = UnityGradientNoise(uv_TexCoord22,temp_output_57_0);
			float gradientNoise37 = UnityGradientNoise(i.uv_texcoord,temp_output_57_0);
			float4 break46 = _NoiseRemap;
			float smoothstepResult52 = smoothstep( 0.0 , 1.0 , abs( (break46.x + (saturate( pow( saturate( ( ( gradientNoise2 + gradientNoise37 ) / 2.0 ) ) , ( 1 * _NoisePower ) ) ) - 0.0) * (break46.y - break46.x) / (1.0 - 0.0)) ));
			float2 temp_cast_5 = (( _Time.y * 0.2 )).xx;
			float2 uv_TexCoord66 = i.uv_texcoord * rotatedValue10.xy + temp_cast_5;
			float gradientNoise59 = GradientNoise(uv_TexCoord66,( 1 * _BaseScale ));
			gradientNoise59 = gradientNoise59*0.5 + 0.5;
			float temp_output_73_0 = ( 1 * _BaseStrength );
			float temp_output_74_0 = ( ( smoothstepResult52 + ( gradientNoise59 * temp_output_73_0 ) ) / ( 1.0 + temp_output_73_0 ) );
			float4 lerpResult48 = lerp( _ColorA , _ColorB , temp_output_74_0);
			o.Albedo = lerpResult48.rgb;
			o.Emission = ( lerpResult48 * ( 1 * _EmissionStrength ) ).rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float eyeDepth81 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
			o.Alpha = saturate( ( ( eyeDepth81 - ( ase_screenPos.w - 1.0 ) ) / ( 1 * _FadeDepth ) ) );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc tessellate:tessFunction 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17700
202;303;1286;706;2507.501;1472.928;6.945048;True;False
Node;AmplifyShaderEditor.CommentaryNode;23;-1893.965,19.12271;Inherit;False;1063.476;643.7437;;6;10;21;20;13;12;11;projection;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector4Node;11;-1827.343,313.9226;Inherit;False;Property;_RotateProjection;Rotate Projection;13;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;92;-1443.822,769.4531;Inherit;False;575.1339;439.7462;;3;25;26;24;Time scale;1,1,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;12;-1550.435,430.5148;Inherit;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.CommentaryNode;96;26.14421,-313.1183;Inherit;False;1728.005;1064.111;;20;42;46;53;45;55;62;61;60;40;39;37;2;38;57;22;52;43;91;19;58;base noises + remap;1,1,1,1;0;0
Node;AmplifyShaderEditor.DegreesOpNode;20;-1451.165,248.9659;Inherit;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleTimeNode;24;-1312.82,818.7554;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-1345.679,965.7283;Inherit;True;Property;_Speed;Speed;15;0;Create;True;0;0;False;0;100;0;0;110;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;13;-1469.343,108.8546;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DegreesOpNode;21;-1281.958,472.0409;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;33.60114,95.0137;Inherit;False;Property;_NoiseScale;Noise Scale;14;0;Create;True;0;0;False;0;1;0.1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotateAboutAxisNode;10;-1133.893,232.1868;Inherit;False;False;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1054.431,806.6627;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;58;54.80758,208.008;Inherit;False;Constant;_ScaleFix;ScaleFix;7;0;Create;True;0;0;False;0;0.005;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;22;165.7283,-86.44421;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;321.3102,76.0901;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;38;235.4025,292.105;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;37;552.9322,140.3965;Inherit;False;Gradient;False;True;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;2;533.9296,-88.2636;Inherit;False;Gradient;False;True;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;876.1355,-41.088;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;61;769.1342,370.0642;Inherit;False;Property;_NoisePower;Noise Power;8;0;Create;True;0;0;False;0;1;0;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;60;811.0184,233.4825;Inherit;False;Constant;_Vector0;Vector 0;7;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleDivideOpNode;40;1054.305,-92.24561;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;55;1183.771,17.13199;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;93;-485.8905,1121.972;Inherit;False;1719.879;746.2843;;11;59;66;70;73;65;71;72;64;67;63;68;more?? noise?? ok;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;1068.487,254.5533;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;53;1260.702,199.7024;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;45;497.764,538.0533;Inherit;False;Property;_NoiseRemap;Noise Remap;5;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;68;-372.6794,1541.362;Inherit;False;Constant;_Float1;Float 1;9;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;91;1382.885,317.3495;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;64;-125.2009,1709.189;Inherit;False;Property;_BaseScale;BaseScale;9;0;Create;True;0;0;False;0;5;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;46;707.3776,545.8915;Inherit;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;-183.6383,1438.54;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;63;-61.46985,1582.956;Inherit;False;Constant;_Vector1;Vector 1;7;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;184.3454,1665.439;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;501.3871,1673.977;Inherit;False;Property;_BaseStrength;BaseStrength;10;0;Create;True;0;0;False;0;5;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;72;565.1183,1547.744;Inherit;False;Constant;_Vector2;Vector 2;7;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCRemapNode;42;1098.91,476.873;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;66;57.54749,1339.424;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;59;380.4951,1436.805;Inherit;False;Gradient;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;94;3527.54,943.1583;Inherit;False;1188.21;792.6015;;10;90;86;85;89;81;88;84;87;83;82;depth fade;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;810.9336,1630.227;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;43;1362.618,480.6297;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;52;1518.939,549.4159;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;82;3531.292,1545.486;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;70;1043.406,1447.719;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;44;1988.578,-588.9939;Inherit;False;1290.008;571.0448;;6;31;30;32;29;33;28;Displacement;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;69;1898.445,559.1172;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;83;3758.098,1437.246;Inherit;False;FLOAT;1;0;FLOAT;0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleAddOpNode;75;1921.359,788.0226;Inherit;False;2;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;95;2618.344,904.9784;Inherit;False;628.9045;349.5235;;3;79;78;77;Emissive;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenDepthNode;81;3633.483,1121.271;Inherit;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;3939.249,1649.334;Inherit;False;Property;_FadeDepth;Fade Depth;12;0;Create;True;0;0;False;0;1;0;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;87;3989.169,1506.643;Inherit;False;Constant;_Vector4;Vector 4;7;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleSubtractOpNode;84;4008.11,1311.848;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;74;2088.544,596.2337;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;51;2411.218,211.2366;Inherit;False;622.2701;519.4611;;3;50;48;49;Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.NormalVertexDataNode;28;2051.068,-350.7431;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;2307.357,-309.2369;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;89;4251.623,1573.893;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;78;2735.059,1089.858;Inherit;False;Property;_EmissionStrength;Emission Strength;11;0;Create;True;0;0;False;0;5;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;33;2465.5,-189.0211;Inherit;False;Constant;_HeightDisplacement;Height Displacement;3;0;Create;True;0;0;False;0;0,3;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;77;2787.432,947.1666;Inherit;False;Constant;_Vector3;Vector 3;7;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ColorNode;49;2558.699,278.4999;Inherit;False;Property;_ColorA;ColorA;7;0;Create;True;0;0;False;0;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;50;2520.828,554.9103;Inherit;False;Property;_ColorB;ColorB;6;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;85;4184.287,1205.464;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;86;4336.201,1255.114;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;79;3049.887,1014.417;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;48;2798.043,461.2903;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;30;2041.042,-514.471;Inherit;False;Object Position;-1;;1;b9555b68a3d67c54f91597a05086026a;0;0;4;FLOAT3;7;FLOAT;0;FLOAT;4;FLOAT;5
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;2663.408,-310.4399;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;90;4474.35,1101.057;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;3329.419,453.5893;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;2858.803,-511.2657;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;5341.792,419.067;Float;False;True;-1;6;ASEMaterialInspector;0;0;Standard;Clouds;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;True;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;0;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;12;0;11;0
WireConnection;20;0;11;0
WireConnection;21;0;12;3
WireConnection;10;0;13;0
WireConnection;10;1;21;0
WireConnection;10;2;20;0
WireConnection;10;3;21;0
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;22;0;10;0
WireConnection;22;1;25;0
WireConnection;57;0;19;0
WireConnection;57;1;58;0
WireConnection;37;0;38;0
WireConnection;37;1;57;0
WireConnection;2;0;22;0
WireConnection;2;1;57;0
WireConnection;39;0;2;0
WireConnection;39;1;37;0
WireConnection;40;0;39;0
WireConnection;55;0;40;0
WireConnection;62;0;60;1
WireConnection;62;1;61;0
WireConnection;53;0;55;0
WireConnection;53;1;62;0
WireConnection;91;0;53;0
WireConnection;46;0;45;0
WireConnection;67;0;24;0
WireConnection;67;1;68;0
WireConnection;65;0;63;1
WireConnection;65;1;64;0
WireConnection;42;0;91;0
WireConnection;42;3;46;0
WireConnection;42;4;46;1
WireConnection;66;0;10;0
WireConnection;66;1;67;0
WireConnection;59;0;66;0
WireConnection;59;1;65;0
WireConnection;73;0;72;1
WireConnection;73;1;71;0
WireConnection;43;0;42;0
WireConnection;52;0;43;0
WireConnection;70;0;59;0
WireConnection;70;1;73;0
WireConnection;69;0;52;0
WireConnection;69;1;70;0
WireConnection;83;0;82;4
WireConnection;75;1;73;0
WireConnection;84;0;83;0
WireConnection;74;0;69;0
WireConnection;74;1;75;0
WireConnection;29;0;28;0
WireConnection;29;1;74;0
WireConnection;89;0;87;1
WireConnection;89;1;88;0
WireConnection;85;0;81;0
WireConnection;85;1;84;0
WireConnection;86;0;85;0
WireConnection;86;1;89;0
WireConnection;79;0;77;1
WireConnection;79;1;78;0
WireConnection;48;0;49;0
WireConnection;48;1;50;0
WireConnection;48;2;74;0
WireConnection;32;0;29;0
WireConnection;32;1;33;2
WireConnection;90;0;86;0
WireConnection;76;0;48;0
WireConnection;76;1;79;0
WireConnection;31;0;30;7
WireConnection;31;1;32;0
WireConnection;0;0;48;0
WireConnection;0;2;76;0
WireConnection;0;9;90;0
WireConnection;0;11;31;0
ASEEND*/
//CHKSM=AB82C7E79FFC9FF84DC91C5A324C86E486ECC6CD