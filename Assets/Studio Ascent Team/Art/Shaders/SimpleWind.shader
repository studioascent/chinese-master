// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/SimpleWind"
{
	Properties
	{
		_LightWrapSoft("Light Wrap Soft", Range( 0 , 1)) = 0
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_ColorInner("Color Inner", Color) = (0,0,0,0)
		_ColorOuter("Color Outer", Color) = (1,1,1,0)
		_Mask("Mask", 2D) = "white" {}
		_WindStrengthDirectionMultiplier("WindStrengthDirectionMultiplier", Vector) = (0,0,0,0)
		_BendPower("Bend Power", Range( 1 , 2)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		AlphaToMask On
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float3 worldNormal;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _WindNoiseTexture;
		uniform float _WindSpeed;
		uniform float _WindNoiseScale;
		uniform float _WindNoiseStrength;
		uniform float4 _WindDirection;
		uniform float _WindDirectionStrength;
		uniform float _BendPower;
		uniform float3 _WindStrengthDirectionMultiplier;
		uniform sampler2D _Mask;
		uniform float4 _Mask_ST;
		uniform float4 _ColorInner;
		uniform float4 _ColorOuter;
		uniform float _LightWrapSoft;
		uniform float _Cutoff = 0.5;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 temp_cast_0 = (_WindSpeed).xx;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 appendResult658 = (float4(ase_worldPos.x , ase_worldPos.y , 0.0 , 0.0));
			float2 panner653 = ( 1.0 * _Time.y * temp_cast_0 + ( appendResult658 * _WindNoiseScale ).xy);
			float3 appendResult665 = (float3(_WindDirection.x , _WindDirection.y , _WindDirection.z));
			float3 worldToObjDir667 = mul( unity_WorldToObject, float4( appendResult665, 0 ) ).xyz;
			v.vertex.xyz += ( ( ( (tex2Dlod( _WindNoiseTexture, float4( panner653, 0, 0.0) )*2.0 + -1.0) * _WindNoiseStrength ) + float4( ( worldToObjDir667 * _WindDirectionStrength ) , 0.0 ) ) * pow( v.color.a , _BendPower ) * float4( _WindStrengthDirectionMultiplier , 0.0 ) ).rgb;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_Mask = i.uv_texcoord * _Mask_ST.xy + _Mask_ST.zw;
			float4 tex2DNode833 = tex2D( _Mask, uv_Mask );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 lerpResult786 = lerp( _ColorInner , _ColorOuter , i.vertexColor.a);
			float4 MainColour790 = lerpResult786;
			float3 ase_worldNormal = i.worldNormal;
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult628 = dot( ase_vertexNormal , ase_worldlightDir );
			c.rgb = ( ( ase_lightColor * ase_lightAtten ) * MainColour790 * saturate( ( ( dotResult628 * _LightWrapSoft ) + ( 1.0 - _LightWrapSoft ) ) ) * tex2DNode833.r ).rgb;
			c.a = 1;
			clip( ( 0.0 * tex2DNode833.a ) - _Cutoff );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.vertexColor = IN.color;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17700
202;73;1324;544;2294.244;-2993.379;3.678888;True;False
Node;AmplifyShaderEditor.CommentaryNode;661;-1402.962,4373.278;Inherit;False;2045.57;1068.656;;21;824;659;829;664;830;843;845;649;667;839;831;844;655;665;653;662;651;842;658;650;657;Wind;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;657;-1352.961,4764.436;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;650;-1090.852,4931.442;Float;False;Global;_WindNoiseScale;_WindNoiseScale;8;0;Create;True;0;0;False;0;-0.15;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;658;-1076.055,4761.853;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;842;-1076.286,5073.493;Float;False;Global;_WindSpeed;_WindSpeed;8;0;Create;True;0;0;False;0;-0.15;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;651;-859.719,4772.989;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;788;-1652.692,3351.499;Inherit;False;1601.547;592.5758;;11;616;615;629;612;630;628;626;775;577;559;579;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector4Node;662;-1327.128,4527.232;Float;False;Global;_WindDirection;_WindDirection;25;0;Create;True;0;0;False;0;0,0,0,0;-0.9178746,0,-0.396871,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;653;-685.6692,4716.488;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;655;-469.7069,4627.61;Inherit;True;Global;_WindNoiseTexture;_WindNoiseTexture;2;0;Create;True;0;0;False;0;-1;None;6373380e25671b142917e356b5f57d76;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;775;-1499.047,3497.607;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;626;-1508.872,3652.597;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;665;-1010.133,4523.019;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;844;-328.6581,4545.825;Inherit;False;Global;_WindDirectionStrength;_WindDirectionStrength;14;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;831;-242.6731,4921.411;Inherit;False;Global;_WindNoiseStrength;_WindNoiseStrength;14;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;839;-151.2467,4671.271;Inherit;False;3;0;COLOR;0,0,0,0;False;1;FLOAT;2;False;2;FLOAT;-1;False;1;COLOR;0
Node;AmplifyShaderEditor.TransformDirectionNode;667;-703.2418,4469.938;Inherit;False;World;Object;False;Fast;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;836;-1143.442,2804.656;Inherit;False;1089.905;461.0059;;5;787;784;785;786;790;Basic Colour;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;630;-1457.443,3834.097;Float;False;Property;_LightWrapSoft;Light Wrap Soft;0;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;628;-1076.902,3654.298;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;629;-811.5086,3803.079;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;785;-780.7365,3058.661;Float;False;Property;_ColorOuter;Color Outer;4;0;Create;True;0;0;False;0;1,1,1,0;1,0.8638201,0.06132074,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;612;-798.9026,3685.298;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;830;-653.1415,5130.66;Inherit;False;Property;_BendPower;Bend Power;9;0;Create;True;0;0;False;0;0;1.536;1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;784;-817.6515,2854.656;Float;False;Property;_ColorInner;Color Inner;3;0;Create;True;0;0;False;0;0,0,0,0;0.7264151,0.2086241,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;787;-1093.442,2950.638;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;843;91.25764,4666.908;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;845;-28.71838,4466.948;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;649;-619.4877,4942.785;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;824;-570.2093,5237.964;Inherit;False;Property;_WindStrengthDirectionMultiplier;WindStrengthDirectionMultiplier;8;0;Create;True;0;0;False;0;0,0,0;1,1,0.2;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LightAttenuation;577;-699.949,3517.222;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;615;-494.5247,3737.91;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;786;-506.7853,2984.83;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;829;-230.6413,4991.56;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;579;-682.3878,3390.037;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;664;209.2905,4561.996;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;838;203.7167,3782.56;Inherit;False;843.8698;534.7266;;5;821;820;833;835;834;Opacity;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;790;-296.5366,2988.255;Float;False;MainColour;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;616;-309.4009,3759.199;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;833;447.8312,4093.695;Inherit;True;Property;_Mask;Mask;7;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;559;-299.094,3423.558;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;659;287.2234,4792.393;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;834;878.5865,3944.853;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;820;253.7167,3841.766;Inherit;False;Property;_FadeDistMin;FadeDistMin;5;0;Create;True;0;0;False;0;0;0.57;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;835;516.4172,3832.56;Inherit;False;FadeNearCamera;-1;;1;9cdfc5729c2790a45bf4f599eb70d667;0;0;0
Node;AmplifyShaderEditor.WireNode;837;955.7147,4573.855;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;555;985.1319,3544.928;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;821;257.7301,3939.328;Inherit;False;Property;_FadeDistMax;FadeDistMax;6;0;Create;True;0;0;False;0;1;1.82;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;114;1531.729,3506.598;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;Custom/SimpleWind;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;True;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;100;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Spherical;False;Relative;0;;1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;658;0;657;1
WireConnection;658;1;657;2
WireConnection;651;0;658;0
WireConnection;651;1;650;0
WireConnection;653;0;651;0
WireConnection;653;2;842;0
WireConnection;655;1;653;0
WireConnection;665;0;662;1
WireConnection;665;1;662;2
WireConnection;665;2;662;3
WireConnection;839;0;655;0
WireConnection;667;0;665;0
WireConnection;628;0;775;0
WireConnection;628;1;626;0
WireConnection;629;0;630;0
WireConnection;612;0;628;0
WireConnection;612;1;630;0
WireConnection;843;0;839;0
WireConnection;843;1;831;0
WireConnection;845;0;667;0
WireConnection;845;1;844;0
WireConnection;615;0;612;0
WireConnection;615;1;629;0
WireConnection;786;0;784;0
WireConnection;786;1;785;0
WireConnection;786;2;787;4
WireConnection;829;0;649;4
WireConnection;829;1;830;0
WireConnection;664;0;843;0
WireConnection;664;1;845;0
WireConnection;790;0;786;0
WireConnection;616;0;615;0
WireConnection;559;0;579;0
WireConnection;559;1;577;0
WireConnection;659;0;664;0
WireConnection;659;1;829;0
WireConnection;659;2;824;0
WireConnection;834;1;833;4
WireConnection;837;0;659;0
WireConnection;555;0;559;0
WireConnection;555;1;790;0
WireConnection;555;2;616;0
WireConnection;555;3;833;1
WireConnection;114;10;834;0
WireConnection;114;13;555;0
WireConnection;114;11;837;0
ASEEND*/
//CHKSM=9619FEEE5ADE848B8CA721A90351AB5E2BDA51FF