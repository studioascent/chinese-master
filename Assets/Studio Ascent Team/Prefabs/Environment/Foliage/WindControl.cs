﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindControl : MonoBehaviour
{

    [Range(0f, 1f)] public float windDirectionStrength;
    [Range(0f, 1f)] public float windNoiseStrength;
    [Range(0f, 1f)]public float windSpeed;
    [Range(0f, 1f)] public float windNoiseScale = 0.15f;

    public Texture2D windNoiseTexture;

    private void Start()
    {
        OnValidate();
    }

    void OnValidate()
    {
        UpdateWind(true);
    }

    void UpdateWind(bool updateTexture = false)
    {
        Shader.SetGlobalVector("_WindDirection", transform.forward);
        Shader.SetGlobalFloat("_WindNoiseScale", windNoiseScale);
        Shader.SetGlobalFloat("_WindNoiseStrength", windNoiseStrength);
        Shader.SetGlobalFloat("_WindSpeed", windSpeed);
        Shader.SetGlobalFloat("_WindDirectionStrength", windDirectionStrength);
        if(updateTexture)
            Shader.SetGlobalTexture("_WindNoiseTexture", windNoiseTexture);
    }

    // Update is called once per frame
    void Update ()
    {
        UpdateWind(false);
    }

#if UNITY_EDITOR

    void OnDrawGizmos()
    {
       UnityEditor.Handles.ArrowHandleCap(0, transform.position, transform.rotation, windDirectionStrength * 10f,EventType.Repaint);
    }
#endif
}
